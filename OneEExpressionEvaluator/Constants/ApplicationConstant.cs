﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignment.OneEExpressionEvaluator.Constants
{
    public class ApplicationConstant
    {
        public static readonly Dictionary<string, int> ExpressionSupported = new Dictionary<string, int>()
        {
            {"+",1},
            {"-",2},
            {"*",3},
            {"/",4}
        };
    }
}
