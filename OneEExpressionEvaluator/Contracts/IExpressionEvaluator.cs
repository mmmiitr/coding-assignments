﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignment.OneEExpressionEvaluator
{
    public interface IExpressionEvaluator
    {
        OutPut EvaluateExpression(string expression);
    }
}
