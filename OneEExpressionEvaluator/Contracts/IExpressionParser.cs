﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignment.OneEExpressionEvaluator
{
    public interface IExpressionParser
    {
        Stack<string> ParseGivenExpression(string expression); 
    }
}
