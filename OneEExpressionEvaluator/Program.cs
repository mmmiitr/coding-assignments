﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignment.OneEExpressionEvaluator
{
    class Program
    {
        static void Main(string[] args)
        {            
            expressionEval = new ExpressionEvaluator(new ExpressionParser(), new SingleOperationEvaluator());
            ExpressionEvaluationLoop();            
        }

        static IExpressionEvaluator expressionEval;

        static void ExpressionEvaluationLoop()
        {
            Console.WriteLine("Please input the string expression to compute");
            Console.WriteLine("String expression must contain space to distinguish between different element eg: 2*2 should be witten as 2 * 2");
            var stringToCompute = Console.ReadLine();
            OutPut outPut = expressionEval.EvaluateExpression(stringToCompute);
            if (outPut != null)
            {
                Console.WriteLine(outPut.IsSuccess ? outPut.OutPutValue.ToString() : outPut.Error);
            }
            else
            {
                Console.WriteLine("Failed to calculate the expression");
            }
            Console.WriteLine("Press escape to exit or any other key to continue...");
            if (Console.ReadKey().Key != ConsoleKey.Escape)
            {
                ExpressionEvaluationLoop();
            }             
        }       
    }
    
}
