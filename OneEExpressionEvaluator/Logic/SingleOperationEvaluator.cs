﻿using CodingAssignment.OneEExpressionEvaluator.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignment.OneEExpressionEvaluator
{
    public class SingleOperationEvaluator : ISingleOperationEvaluator
    {
        
        #region ISingleOperationEvaluator Members

        public int EvaluateOneExpression(int first, string operation, int second)
        {
            int outCome = 0;
            int operationVal = ApplicationConstant.ExpressionSupported[operation];
            switch (operationVal)
            {
                case 1:
                    outCome = first + second;
                    break;
                case 2:
                    outCome = first - second;
                    break;
                case 3:
                    outCome = first * second;
                    break;
                case 4:
                    outCome = first / second;
                    break;
            }
            return outCome; 
        }

        #endregion
    }
}
