﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignment.OneEExpressionEvaluator
{
    public class ExpressionParser : IExpressionParser
    {
        #region IExpressionParser Members

        public Stack<string> ParseGivenExpression(string expression)
        {
            Stack<string> expressionStack = null;
            if (!string.IsNullOrEmpty(expression))
            {
                string[] expressionVariables = expression.Split(' ');
                if (expressionVariables != null)
                {
                    expressionStack = new Stack<string>();
                    foreach (var element in expressionVariables)
                    {
                        expressionStack.Push(element);
                    }
                }
            }
            return expressionStack;
        }

        #endregion
    }
}
