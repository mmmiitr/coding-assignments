﻿using CodingAssignment.OneEExpressionEvaluator.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignment.OneEExpressionEvaluator
{
    public class ExpressionEvaluator : IExpressionEvaluator
    {
        IExpressionParser _expressionParser;
        ISingleOperationEvaluator _iSingleOperationEvaluator;
        public ExpressionEvaluator(IExpressionParser expressionParser,ISingleOperationEvaluator singleOperationEvaluator)
        {
            _expressionParser = expressionParser;
            _iSingleOperationEvaluator = singleOperationEvaluator;
        }
        #region IExpressionEvaluator Members

        public OutPut EvaluateExpression(string expression)
        {
            OutPut outPut = null;
            Stack<string> expressionStack = _expressionParser.ParseGivenExpression(expression);
            if (expressionStack != null)
            {
                try
                {
                    var firstVar = Evaluate(null, null, expressionStack);
                    outPut = new OutPut { IsSuccess = true, OutPutValue = firstVar };
                }
                catch (FormatException forEx)
                {
                    outPut = new OutPut { Error = forEx.Message };
                }                
            }
            else
            {
                outPut = new OutPut { Error = "Could not evaluate expression" };
            }
            return outPut;
        }

        #endregion

        private int Evaluate(int? secondNumber, string operation, Stack<string> remainingExpression)
        {
            int num = 0;
            if (remainingExpression == null || remainingExpression.Count == 0)
            {
                return num;
            }
            var secondValue = remainingExpression.Pop();
            string newOperation = null;
            if (remainingExpression.Count > 0)
                newOperation = remainingExpression.Pop();
            int firstNumber = 0;
            if (int.TryParse(secondValue, out firstNumber) && (string.IsNullOrEmpty(newOperation) || ApplicationConstant.ExpressionSupported.ContainsKey(newOperation)))
            {
                //first time
                if (!secondNumber.HasValue && string.IsNullOrEmpty(operation))
                {
                    num = Evaluate(firstNumber, newOperation, remainingExpression);
                }
                else if (string.IsNullOrEmpty(newOperation))
                {
                    num = _iSingleOperationEvaluator.EvaluateOneExpression(firstNumber, operation, secondNumber.Value);
                }
                else
                {
                    int newOperatorVal = ApplicationConstant.ExpressionSupported[newOperation];
                    int oldOperatorVal = ApplicationConstant.ExpressionSupported[operation];

                    if (oldOperatorVal >= newOperatorVal)
                    {
                        remainingExpression.Push(newOperation);
                        num = _iSingleOperationEvaluator.EvaluateOneExpression(firstNumber, operation, secondNumber.Value);
                        remainingExpression.Push(num.ToString());
                        num = Evaluate(null, null, remainingExpression);
                    }
                    else
                    {
                        var remainingExpressionVal = Evaluate(firstNumber, newOperation, remainingExpression);
                        num = _iSingleOperationEvaluator.EvaluateOneExpression(remainingExpressionVal, operation, secondNumber.Value);
                    }
                }
            }
            else
            {
                throw new FormatException(string.Format("Invalid expression, expected a numeric value instead of {0}", secondValue));
            }
            return num;
        }

        
    }
}
