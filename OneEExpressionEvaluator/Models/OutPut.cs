﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignment.OneEExpressionEvaluator
{
    public class OutPut
    {
        public bool IsSuccess { get; set; }
        public int OutPutValue { get; set; }
        public string Error { get; set; }
    }
}
